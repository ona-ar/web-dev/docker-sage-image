ARG WP_IMAGE_TAG=latest
FROM wordpress:$WP_IMAGE_TAG

# Create a user and change WP installation to be owned by the given USER_ID (or 1000)
ARG USER_ID=1000
RUN useradd --shell /bin/bash --uid ${USER_ID} --gid www-data ona

# Install XDebug
RUN pecl install xdebug && \
  docker-php-ext-enable xdebug

# Install composer
COPY ./install-composer.sh /tmp/
RUN cd /tmp && /tmp/install-composer.sh && mv /tmp/composer.phar /usr/bin/composer

# Install Wordpress CLI
RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar && \
  chmod +x wp-cli.phar && \
  mv wp-cli.phar /usr/local/bin/wp

# Install NVM, Node & yarn
ARG NODE_ARGS=--lts
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash && \
  export NVM_DIR="$HOME/.nvm" && \
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" && \
  nvm install ${NODE_ARGS} && \
  npm install --global yarn
